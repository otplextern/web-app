package hello;

import graphql.schema.DataFetcher;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.util.empty.EmptyGraph;
import org.springframework.stereotype.Component;

@Component
public class GraphQLDataFetchers {

    /*public Object helloGradle() {
        Graph graph = EmptyGraph.instance();
        GraphTraversalSource g;
        {
            try {
                g = graph.traversal().withRemote("conf/janusgraph-cql-es.properties");
                Vertex vertex4 = g.addV("specObject").property("requirementID","1g").property("description", "this is test requiremnt 44").property("testcaseAdded", false).next();
                return g.V().has("requirementID", "1b").values("description").next();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Object();
    }*/
    Graph graph = EmptyGraph.instance();
    GraphTraversalSource g;

    public DataFetcher getSpecObjectByIDDataFetcher() {
        return dataFetchingEnvironment -> {
            String requirementID = dataFetchingEnvironment.getArgument("requirementID");
            {
                try {
                    g = graph.traversal().withRemote("conf/janusgraph-cql-es.properties");
                    Vertex vertex4 = g.addV("specObject").property("requirementID","1g").property("description", "this is test requiremnt 44").property("testcaseAdded", false).next();
                    return g.V().has("requirementID", requirementID).values("description").next();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        };
    }

    public DataFetcher getSpecRelationBySourceDataFetcher() {
        return dataFetchingEnvironment -> {
            String sourceID = dataFetchingEnvironment.getArgument("sourceID");
            {
                try {
                    g = graph.traversal().withRemote("conf/janusgraph-cql-es.properties");
                    Vertex r1 = g.addV("specRelation").property("sourceID","1g").property("destinationID", "1d").property("relation","child").next();
                    return g.V().has("sourceID", sourceID).values("relation").next();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        };
    }

}
