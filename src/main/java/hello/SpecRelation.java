package hello;

public class SpecRelation {
    private String relationType;
    private String sourceID;
    private String destinationID;

    public SpecRelation(String relationType,String sourceID, String destinationID) {
        this.relationType = relationType;
        this.sourceID = sourceID;
        this.destinationID = destinationID;
    }

    public String getRelationType() {
        return relationType;
    }

    public String getSourceID() {
        return sourceID;
    }

    public String getDestinationID() {
        return destinationID;
    }
}

