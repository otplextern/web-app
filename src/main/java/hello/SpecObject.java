package hello;

import java.util.ArrayList;
import java.util.List;

public class SpecObject {

    private String requirementID;
    private String description;
    private Boolean testcaseAdded;
    //private List<SpecRelation> relations;

    public SpecObject(String requirementID, String description, Boolean testcaseAdded) {
        this.requirementID = requirementID;
        this.description = description;
        this.testcaseAdded = testcaseAdded;
        //this.relations = relations;
    }

    public String getRequirementID() {
        return requirementID;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getTestcaseAdded() {
        return testcaseAdded;
    }
    /*public List getRelations() {
        return relations;
    }*/

}
